cmake_minimum_required(VERSION 3.8)
project(TelephoneDirectory)

set (CMAKE_CXX_STANDART 15)

find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)
find_package(Qt5Widgets REQUIRED)

set(SOURCE_FILE main.cpp mainwindow.cpp mainwindow.h)
set(CMAKE_AUTOMOC ON)

add_executable(TelephoneDirectory "main.cpp" "mainwindow.cpp" "mainwindow.h") 

target_link_libraries(${PROJECT_NAME} Qt5::Core)
target_link_libraries(${PROJECT_NAME} Qt5::Gui)
target_link_libraries(${PROJECT_NAME} Qt5::Widgets)

