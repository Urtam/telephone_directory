#include "mainwindow.h"
#include <QGridLayout>
#include <QCoreApplication>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setWindowTitle(tr("Telefon Directory"));

    QWidget *wid = new QWidget;
    setCentralWidget(wid);

    treeView -> setHeaderHidden(1);

    connect(button, SIGNAL(clicked()), this, SLOT(Show()));

    QGridLayout *grid = new QGridLayout;
    grid -> addWidget(box, 0, 0);
    grid -> addWidget(button, 0, 1);
    grid -> addWidget(treeView, 1, 0);
    wid -> setLayout(grid);


    file.setFileName(QFileDialog::getOpenFileName(nullptr, "", "C:\\TelephoneDirectory", "*.json"));
    if (file.open(QIODevice::ReadOnly|QFile::Text))
    {
        Jdoc = QJsonDocument::fromJson(QByteArray(file.readAll()), &JdocError);
        file.close();

        if (JdocError.errorString().toInt() == QJsonParseError::NoError)
        {
            JdocArray = QJsonValue(Jdoc.object().value("PhoneBook")).toArray();
            root = QJsonObject(JdocArray.at(0).toObject());
            for (int i = 0; i < root.count(); i++) {
                box->addItem(root.keys().at(i));
            }
        }
    }
}

void MainWindow::Show()
{
    QStandardItemModel *model = new QStandardItemModel(nullptr);
    QStandardItem *root = model -> invisibleRootItem();

    for (int i = 0; i < JdocArray.count(); i++)
    {
        QStandardItem *header = new QStandardItem(box->currentText());
        root->appendRow(header);
        JdocAr = QJsonValue(QJsonObject(JdocArray.at(i).toObject()).value(box->currentText())).toArray();
        for (int j = 0; j < JdocAr.count(); j++)
        {
            name = new QStandardItem(JdocAr.at(j).toObject().value("Name").toString());
            header -> appendRow(name);
            age = new QStandardItem(JdocAr.at(j).toObject().value("Age").toString());
            name -> appendRow(age);
            tel = new QStandardItem(JdocAr.at(j).toObject().value("Tel").toString());
            name -> appendRow(tel);
        }
    }
    treeView -> setModel(model);
}

MainWindow::~MainWindow()
{

}
