#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QFile>
#include <QFileDialog>
#include <QPushButton>
#include <QComboBox>
#include <QTreeView>
#include <QStandardItemModel>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QJsonDocument Jdoc;
    QJsonArray JdocArray;
    QJsonArray JdocAr;
    QJsonParseError JdocError;

    QFile file;

    QStandardItem *name;
    QStandardItem *age;
    QStandardItem *tel;

private:   
    QPushButton *button = new QPushButton("Show", this);
    QComboBox *box = new QComboBox(this);
    QTreeView *treeView = new QTreeView(this);
    QJsonObject root;

private slots:
    void Show();
};

#endif // MAINWINDOW_H
